# README  #

This page contains general information how to launch applications with rules engines. 
This tips are applicable to all engines. Specific information will be given in the README file of the given project which this information is related to (especially the information about necessary files which have to be loaded into phone/tablet internal storage).

### Downloading the code from the repository ###
* Run git command line, move to the directory where you want to place the code and clone the project by launching the command given on the Overview page of the repository.
* Run Android Studio and import existing project, indicating the build.gradle script in the import wizard (this script is inside the 'app' directory of the cloned project).
* If you get "Gradle settings for this project are not configured yet", click "OK".
* New project window should open. If you get the following error: 
```
#!console

Error:(1, 0) Plugin with id 'com.android.application' not found.
```
click "Open File" and paste the following code just after the "apply plugin: 'com.android.application'":

```
#!gradle

buildscript {
    repositories {
        jcenter()
    }

    dependencies {
        classpath "com.android.tools.build:gradle:2.3.3"
    }
}
```

and click "Try again". 

* You may get error like
```
#!console

Error:Failed to find target with hash string 'android-22' in: some_path
```
Then try to modify the Gradle script by changing compileSdkVersion and targetSdkVersion to something you have on your computer, e.g. 23 or 26.

* Then you may get an error related to not compatible version of SDK Build Tools (minimum required is 25.0.0). If there is a link "Update Build Tools version", click it.

* Android studio/Gradle may synchronize or index scripts and files, so please wait if something happens. At the end the project should be ready to run.
* In the source code comment all lines which run the "Monitor" async task or open a text file in the "benchmark" folder. All projects were used to measure rule engines efficiency so they contain some instructions measuring particular parameters and saving them to a text file. Most exceptions are not catched so they may make the app turn off.

I wish you nice and pleasant work with rules engines.